<?php

/**
 * @file
 * Admin page callbacks for the Pure Storage module.
 */

/**
 * Admin callback for the main config form
 */
function _pure_storage_admin() {

  $form = array();

  $form['pure_storage_base_url'] = array(
    '#title' => t('Base URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('pure_storage_base_url', $GLOBALS['base_url']),
    '#description' => t("The URL to pass to Pure Storage's content syndication service. By default this is the current site URL, but on a development site you may wish to pass an alternative value to mimic the live site."),
  );

  $form['pure_storage_suppress_title'] = array(
    '#title' => t('Suppress page title'),
    '#description' => t("The content returned from the Pure Storage syndication service renders Drupal's title superfluous on most themes. Enable this setting to suppress the display of the title on this page."),
    '#type' => 'checkbox',
    '#default_value' => variable_get('pure_storage_suppress_title', TRUE),
  );

  return system_settings_form($form);

}
